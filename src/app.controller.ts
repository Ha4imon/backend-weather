import { Controller, Get, Query, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(private readonly httpService: HttpService) {}

  @Get()
  async getWeather(@Query() query): Promise<AxiosResponse> {
    try {
      const { data } = await this.httpService.axiosRef.get('/v2/forecast', {
        params: { ...query },
      });

      return data;
    } catch (e) {
      throw new Error('Error');
    }
  }
}
