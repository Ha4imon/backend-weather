import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

import configuration from '../config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [configuration] }),
    HttpModule.register({
      baseURL: process.env.YANDEX_API,
      timeout: 5000,
      headers: {
        'X-Yandex-API-Key': process.env.YANDEX_API_KEY,
      },
    }),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
